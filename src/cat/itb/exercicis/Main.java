package cat.itb.exercicis;

public class Main {

    public static void main(String[] args) {
        Taquilla taquilla= new Taquilla(3);
        Thread t1=new Thread(()-> taquilla.reservarEntrades(),"Joan");
        Thread t2=new Thread(()-> taquilla.reservarEntrades(),"Elena");
        Thread t3=new Thread(()-> taquilla.reservarEntrades(),"Albert");
        Thread t4=new Thread(()-> taquilla.reservarEntrades(),"Penny");
        Thread t5=new Thread(()-> taquilla.reservarEntrades(),"Ana");

        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
    }



}
