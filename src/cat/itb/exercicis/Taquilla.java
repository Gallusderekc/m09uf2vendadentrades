package cat.itb.exercicis;

public class Taquilla {
    private int entrades;

    public Taquilla(int num) {entrades = num;}

    public synchronized void reservarEntrades() {
        int num = (int) (Math.random() * 5 +1);
        if (num > 4 && num > entrades) {
            System.out.println(Thread.currentThread().getName() + " No pots comprar més de 4 entrades");
        } else if (num <= entrades) {
            System.out.println(Thread.currentThread().getName() + " Ha comprat " + num + " entrades pel concert de Judas Priest.");
            entrades-=num;
            //numEntrades
            if (entrades == 0) System.out.println("S'han acabat les entrades.");
        } else {
            System.out.println("Ho sento " + Thread.currentThread().getName() + " no queden entrades suficients");
        }
    }
}
